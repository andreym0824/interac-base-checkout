from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class DriverFactory:
	"""docstring for DriverFactory"""
	def __init__(self, arg=None):
		super(DriverFactory, self).__init__()
		self.driver = None
		self.driverName = arg

	def getDriver(self):
		try:
			options = webdriver.ChromeOptions()
			options.add_argument('--ignore-certificate-errors')
			options.add_argument('--ignore-ssl-errors')
			self.driver = webdriver.Chrome(chrome_options=options)
		except Exception as e:
			raise e
		
		return self.driver

	def quitDriver(self):
		self.driver.quit()

