from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from datetime import timedelta, datetime
import logging

class BaseInfo(object):
	"""docstring for BaseInfo"""
	def __init__(self):
		super(BaseInfo, self).__init__()
		self.name = "SeleniumInterACCheckoutAutomation"
		self.author = "Damir K"
		self.allowed_domains = ["https://banquenet.td.com"]
		logging.basicConfig(filename='track.log')


class AccountInfo():
	def __init__(self, args=None):
		self.userId = ""
		self.userPassword = ""
		self.invoice = ""
		self.bankAccount = ""
