import threading
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from config.DriverFactory import DriverFactory
from config.Base import AccountInfo, BaseInfo
import time


class DriverBase():	
	"""docstring for DriverBase"""
	def __init__(self, driver, arg):		
		self.currentDriverId = 0		
		self.driver = driver
		self._userInfo = arg
		self._accountInfo = AccountInfo()
		self._baseInfo = BaseInfo()
		#self.element = None
		print(self._userInfo)

	def getLocator(self, mode, key):
		try:
			element = self.driver.find_element(mode, key)
			#WebDriverWait(self.driver, 2000).until(EC.presence_of_element_located((mode, key)))
			#element = self.driver.find_element(mode,key)

		except Exception as e:
			print(e)
			return None
		finally:
			return element

	def proceeding(self, page):
		locators = {}
		self.driver.get(self._baseInfo.allowed_domains[0])
		#self.driver.implicitly_wait(10)
		if page is "login":
			locators["userId"] = self.getLocator(By.XPATH, '//input[@id="username100"]')
			locators["password"] = self.getLocator(By.XPATH, '//input[@id="password"]')
			locators["login"] = self.getLocator(By.CSS_SELECTOR, 'form#loginForm button[type="submit"]')

			if None in [locators["userId"], locators["password"], locators["login"]] :
				return False
			locators["userId"].send_keys(self._accountInfo.userId)
			locators["password"].send_keys(self._accountInfo.userPassword)			
			locators["login"].click()
			self.driver.implicitly_wait(10)
			
		elif page is "myaccount":
			locators["interac-e-transfer-link"] = self.getLocator(By.XPATH, '//nav[4]/ul/li/span/a')
			if locators["interac-e-transfer-link"] is None :
				return False
			locators["interac-e-transfer-link"].click()
			self.driver.implicitly_wait(3)

		elif page is "sendmoney":
			locators["request-money-link"] = self.getLocator(By.XPATH, "//nav[@id='td-nav-left']/ul/li[2]/ul/li[2]/span/span/a/span")
			if locators["request-money-link"] is None:
				return False
			locators["request-money-link"].click()
			self.driver.implicitly_wait(3)

		elif page is "requestmoney":
			locators["add-new-link"] = self.getLocator(By.XPATH, "//a[@id='request-money-step1-add-new-contact']")
			if locators["add-new-link"] is None:
				return False
			locators["add-new-link"].click()
			self.driver.implicitly_wait(1)

		elif page is "addnewuser":
			locators["name"] = self.getLocator(By.ID, "name")
			locators["language"] = self.getLocator(By.ID, "emailLangSelect")
			locators["email"] = self.getLocator(By.ID, "email")
			locators["emailConfirm"] = self.getLocator(By.ID, "confirmEmail")
			locators["phoneNumber"] = self.getLocator(By.NAME, "mobileNumber")
			locators["addBtn"] = self.getLocator(By.ID, "add-contact-add-button")
			
			if None in [locators["name"], locators["language"], locators["email"], locators["emailConfirm"], locators["phoneNumber"], locators["addBtn"]] :
				return False

			locators["name"].send_keys(self._userInfo["name"])
			locators["language"].find_element(By.XPATH, "//option[. = 'English']").click()			
			locators["email"].send_keys(self._userInfo["email"])
			locators["emailConfirm"].send_keys(self._userInfo["email"])
			locators["phoneNumber"].send_keys(self._userInfo["phone"])
			locators["addBtn"].click()
			self.driver.implicitly_wait(2)

		elif page is "addcontact":
			locators["request-money-btn"] = self.getLocator(By.ID, "add-contact-request-money-button")
			if locators["request-money-btn"] is None:
				return False
			locators["request-money-btn"].click()
			self.driver.implicitly_wait(2)

		elif page is "requestmoney-after-add":
			locators["amount"] = self.getLocator(By.ID, "amount")
			locators["invoice"] = self.getLocator(By.ID, "invoiceNumber")
			locators["message"] = self.getLocator(By.ID, "fundTransferMessage")
			locators["choose-account"] = self.getLocator(By.ID, "accountSelect")
			locators["nextBtn"] = self.getLocator(By.ID, "next_button")
			if None in [locators["amount"], locators["invoice"], locators["message"], locators["choose-account"], locators["nextBtn"]] :
				return False
			locators["amount"].send_keys(self._userInfo['amount'])
			locators["invoice"].send_keys()
			locators["message"].send_keys()
			locators["choose-account"].send_keys(self._accountInfo['backAccount'])
			locators["nextBtn"].click()
			self.driver.implicitly_wait(3)

		elif page is "requestmoneyConfirm":
			locators["request-money-btn"] = self.getLocator(By.ID, "submit_button")
			if None is locators["request-money-btn"]:
				return False
			locators["request-money-btn"].click()
			self.driver.implicitly_wait(3)
		elif page is "deleteuser":
			locators["manage-contacts-menu"] = self.getLocator(By.XPATH, '//*[@id="td-nav-left"]/ul/li[2]/ul/li[3]/span/span[2]/a').click()
			self.driver.implicitly_wait(2)
			locators["delete-links"] = self.driver.find_elements(By.XPATH, '//table[@role="presentation"]/tbody/tr[@class="ng-scope"]/td[4]/p/a[2]')
			if None is locators["manage-contacts-menu"]:
				return False
			for deleteLink in locators["delete-links"]:
				deleteLink.click()

		else :
			pass
		return True

Max_Threads_Num = 100

class ThreadBase:
	"""docstring for ThreadBase"""
	def __init__(self, arg=None):
		super(ThreadBase, self).__init__()
		self.threadPool = []
		self.arg = arg

	# def isThreadOver(self):
	# 	if(len(self.threadPool) >= Max_Threads_Num):
	# 		return True
	# 	else:
	# 		return False

	# def addOneToPool(self, thread):
	# 	self.threadPool.append(thread)
	# 	#driver.append(driver)

	# def removeOneFromPool(self, thread):
	# 	if(thread in self.threadPool):
	# 		self.threadPool.remove(thread)
	# 	else :
	# 		print("%s Can't be removed because it isn't in threadPool!" % (thread))

	def automationProcess(self, driver, arg):
		driverBase = DriverBase(driver, arg)
		pages = ["login", "myaccount", "sendmoney", "requestmoney", "addnewuser", "addcontact", "requestmoney-after-add", "requestmoneyConfirm", "deleteuser"]
		for page in pages:
			processResult = driverBase.proceeding(page)
			if(not processResult):
				print("Automation Failed in %s\n"%(page))
				return False
			break
		return True

	# def threadFunc(self, arg):
	# 	userInfo = arg
	# 	threadName = threading.current_thread().name
	# 	driver = DriverFactory(threadName).getDriver()
	# 	self.addOneToPool(threadName)
	# 	if(not self.automationProcess(driver, arg)):
	# 		print("Automation Process Failed")
	# 	driver.quit()
	# 	self.removeOneFromPool(threadName)

	def createThread(self, arg):
		#userInfo = arg
		#threadName = threading.current_thread().name
		driver = DriverFactory().getDriver()
		#self.addOneToPool(threadName)
		if(not self.automationProcess(driver, arg)):
			print("Automation Process Failed")
		driver.quit()
		#self.removeOneFromPool(threadName)
		# if(self.isThreadOver()):
		# 	print("Please wait...")
		# 	return False
		# t = threading.Thread(target=self.threadFunc, args=(arg,))
		# t.daemon = True
		# t.start()		
		# t.join()
		return True

	