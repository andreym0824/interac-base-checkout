import sys
import time
from datetime import timedelta, datetime
import logging
from config.DriverBase import ThreadBase
import argparse
from multiprocessing import Process

parser = argparse.ArgumentParser()

parser.add_argument("-n", "--name", dest="name", default="aaa", help="User Name")
parser.add_argument("-m", "--email", dest="email", default="aaa", help="User Email")
parser.add_argument("-p", "--phone", dest="phone", default="aaa", help="User Phone Number")
parser.add_argument("-a", "--amount", dest="amount", default="111", help="Amount User Entered")

if __name__=="__main__" :
	args = parser.parse_args()
	args = vars(args)
	if '' in args.values():
		print("Not Fully Entered")
	else :
		print("Fully Entered")
		thread = ThreadBase()
		p = Process(target=thread.createThread, args=(args,))
		p.start()
		p.join()

